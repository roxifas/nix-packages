{ fetchFromGitHub, rustPlatform, wayland, pkg-config }:
rustPlatform.buildRustPackage rec {
  pname = "wl-clip-persist";
  version = "0.3.1";

  src = fetchFromGitHub {
    owner = "Linus789";
    repo = pname;
    rev = "6ba11a2aa295d780f0b2e8f005cf176601d153b0";
    hash = "sha256-wg4xEXLAZpWflFejP7ob4cnmRvo9d/0dL9hceG+RUr0=";
  };
  buildInputs = [ wayland.dev ];
  nativeBuildInputs = [ pkg-config ];

  cargoHash = "sha256-vNxNvJ5tA323EVArJ6glNslkq/Q6u7NsIpTYO1Q3GEw=";
}
