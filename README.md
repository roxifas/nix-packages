# Nix packages

This is a personal repo for nix package definitions of software that I wanted to use on NixOS, which is not contained in nixpkgs.

# Contents

- [Streamrip](https://github.com/nathom/streamrip)
- [wl-clip-persist](https://github.com/Linus789/wl-clip-persist)
- [wttrbar](https://github.com/bjesus/wttrbar)
- Packages will be added/removed as I find the need for them, or if they get upstreamed to nixpkgs, etc.
