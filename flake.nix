{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
    flake-utils.url = "github:numtide/flake-utils";
    poetry2nix.url = "github:nix-community/poetry2nix";
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix }:
    { } // (flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ poetry2nix.overlay ];
        };
      in
      {
        apps = flake-utils.lib.mkApp {
          drv = pkgs.streamrip;
        };
        packages = {
          streamrip = pkgs.callPackage ./streamrip { };
          wl-clip-persist = pkgs.callPackage ./wl-clip-persist { };
          wttrbar = pkgs.callPackage ./wttrbar { };
          tree-sitter-nim = pkgs.callPackage ./tree-sitter-nim { };
        };
      }));
}
