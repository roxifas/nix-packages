{ lib, poetry2nix }: poetry2nix.mkPoetryApplication
{
  projectDir = builtins.fetchTarball {
    url = "https://github.com/nathom/streamrip/archive/refs/tags/v1.9.7.tar.gz";
    sha256 = "sha256:0kj6r6sz0g65qz3akd3a22lcm4hsfw0w1vw3y6qlmk0khxwgfxj7";
  };
  overrides = poetry2nix.overrides.withDefaults
    (self: super: {
      # https://github.com/nix-community/poetry2nix/issues/669
      windows-curses = null;
      # Like with windows-curses this dependency is windows only
      pick = null;
    });
  meta = {
    mainProgram = "rip";
    homepage = "https://github.com/nathom/streamrip";
    description = "A scriptable stream downloader for Qobuz, Tidal, Deezer and SoundCloud.";
    license = lib.licenses.gpl3;
  };
}
