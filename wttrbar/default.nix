{ fetchFromGitHub, rustPlatform }:

rustPlatform.buildRustPackage rec {
  pname = "wttrbar";
  version = "0.4.0";

  src = fetchFromGitHub {
    owner = "bjesus";
    repo = pname;
    rev = version;
    hash = "sha256-697LoXu6x8ODQa7tG/NqpSqnLJgM765wBFFnKyul7uI=";
  };

  cargoHash = "sha256-sxZ4R7QXQSuNFNRuOI/omON6QmQ0DTKQvjHy1BcvXAA=";
}
