{ tree-sitter, fetchFromGitHub, ... }: tree-sitter.buildGrammar {
  language = "nim";
  version = "0.0.3";
  src = fetchFromGitHub {
    owner = "alaviss";
    repo = "tree-sitter-nim";
    rev = "0.0.3";
    hash = "sha256-DyOtseKoFZsVKDC8gCuPjN9S2UupP33gcRXvofzxXxk=";
  };
  meta.homepage = "https://github.com/alaviss/tree-sitter-nim";
}
